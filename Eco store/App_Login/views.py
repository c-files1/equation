from django.shortcuts import render,redirect, HttpResponseRedirect
from django.urls import reverse
from django.contrib import auth
from django.http import HttpResponse

# Authentication
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Forms & Models
from App_Login.forms import SignUpForm, ProfileForm
from App_Login.models import Profile

# Messages
from django.contrib import messages

# Create your views here.

def sign_up(request):
    form = SignUpForm()
    # registered = False

    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()
            # registered = True
            messages.success(request, 'Account created successfully')
            return HttpResponseRedirect(reverse('App_Login:login'))

    
    return render(request, 'App_Login/sign_up.html', context={'form':form})

def login_user(request):
    if request.method=="POST":
        email=request.POST['email']
        password=request.POST['password']
        user = auth.authenticate(email=email,password=password)
        if user is not None:
            auth.login(request,user)
            return redirect('/')
        else:
            messages.success(request,'Credentials Invalid')
            return render(request, 'App_Login/login.html',)
    else:
        return render(request, 'App_Login/login.html',)

@login_required
def logout_user(request):
    logout(request)
    messages.warning(request, 'You have been logged out')
    return HttpResponseRedirect(reverse('App_Shop:home'))

@login_required
def user_profile(request):
    profile = Profile.objects.get(user=request.user)
    form = ProfileForm(instance=profile)

    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=profile)

        if form.is_valid():
            form.save()
            messages.success(request, 'Profile updated successfully')
            form = ProfileForm(instance=profile)

    return render(request, 'App_Login/change_profile.html', context={'form':form})
