from django.forms import ModelForm
from App_Login.models import User, Profile

from django.contrib.auth.forms import UserCreationForm
from django import forms

#UserProfileForm
class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        exclude = ('user',)

#SignUpForm
class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email','password1','password2',)
    email=forms.CharField(widget=forms.EmailInput(attrs={
        'placeholder':'Enter your email',
        'class':'box'
    }))
    password1=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your password',
        'class':'box'
    }))
    password2=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Confirm password',
        'class':'box'
    }))
    

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].help_text = "" 