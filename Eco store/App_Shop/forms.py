from django import forms
from .models import Product, Category,Business

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'preview_text', 'detail_text', 'category', 'price', 'old_price', 'mainimage']
    name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter name of product',
        'class':'box'
    }))
    preview_text=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Preview text',
        'class':'box'
    }))
    detail_text=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Product Description',
        'class':'box'
    }))
    price=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Product New Price',
        'class':'box'
    }))
    old_price=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Product previous Price',
        'class':'box'
    }))
   
    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        # Restrict category choices if needed (optional)
        self.fields['category'].queryset = Category.objects.all()  # Get all categories

class BusinessForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = ['name','owner', 'business_type', 'registration_number', 'tax_id', 'street_address', 'city', 'division', 'country']
    name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter Business name',
        'class':'box'
    }))
    owner=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your name',
        'class':'box'
    }))
    business_type=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter Business type',
        'class':'box'
    }))
    registration_number=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter reg no',
        'class':'box'
    }))
    tax_id=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your tax ID',
        'class':'box'
    }))
    street_address=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter street number',
        'class':'box'
    }))
    city=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter current city',
        'class':'box'
    }))
    division=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter division',
        'class':'box'
    }))
    country=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter Country',
        'class':'box'
    }))
    