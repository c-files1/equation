from django.urls import path
from App_Shop import views

app_name = 'App_Shop'

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('add_product', views.add_product, name='add_product'),
    path('search', views.search, name='search'),
    path('register_business', views.register_business, name='register_business'),
    path('product/<pk>/', views.ProductDetail.as_view(), name='product_detail'),
    path('about', views.about, name='aboutpage')
]