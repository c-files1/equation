from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import ProductForm,BusinessForm

# Import Views
from django.views.generic import ListView, DetailView

# Import Models
from App_Shop.models import Product,Category

# Mixins
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

class Home(ListView):
    model = Product
    template_name = 'App_Shop/home.html'

class ProductDetail(LoginRequiredMixin, DetailView):
    model = Product
    template_name = 'App_Shop/product_detail.html'


def add_product(request):
   
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)  # Include uploaded files
        if form.is_valid():
            new_product = form.save()  # Use form.save() for validated data
            messages.success(request, 'Product was added succesfully ')

            # Success message or redirect to product detail page
            return redirect('/') # Redirect with ID

        else:
            # Handle form errors (optional: display in the template)
            return render(request, 'App_Shop/add_product.html', {'form': form, 'categories': Category.objects.all()})
    else:
        form = ProductForm()  # Create an empty form for initial display
    return render(request, 'App_Shop/add_product.html', {'form': form, 'categories': Category.objects.all()})

def register_business(request):
    if request.method=='POST':
        form = BusinessForm(request.POST, request.FILES)
        if form.is_valid():
            new_business=form.save()
            messages.success(request, 'Business was registered succesfully ')
            return redirect('/')
        
        
    else:    
        form = BusinessForm()
        return render(request,'App_Shop/register_business.html',{'form':form})
    

def search(request):
    if request.method=='POST':
        searched=request.POST['search']
        searched=Product.objects.filter(name__icontains=searched)
        if not searched:
            messages.success(request,'That product does not exist, please try again')
            return render(request,'App_Shop/search.html',{})
        else:
            return render(request,'App_Shop/search.html',{'searched':searched})
    else:    
        return render(request,'App_Shop/search.html')
    
def about(request):
    return render(request, 'about.html')