# Generated by Django 5.0.3 on 2024-03-27 08:41

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("App_Shop", "0003_alter_product_old_price"),
    ]

    operations = [
        migrations.CreateModel(
            name="Business",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
            ],
            options={
                "verbose_name_plural": "Businesses",
            },
        ),
    ]
