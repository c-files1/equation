# Generated by Django 5.0.3 on 2024-03-27 09:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("App_Shop", "0006_rename_state_business_division"),
    ]

    operations = [
        migrations.AddField(
            model_name="business",
            name="owner",
            field=models.CharField(default="", max_length=120),
        ),
    ]
