from django.contrib import admin
from App_Shop.models import Category, Product,Business

# Register your models here.

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Business)
