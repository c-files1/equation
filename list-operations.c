#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes to define 
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int newValue);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

// Function to create a new node
//this is a commment
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

// Function to print all nodes in the list
void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

// Function to delete a node with a specific key
void deleteByKey(struct Node **head, int key) {
    struct Node *current = *head;
    struct Node *previous = NULL;

    while (current != NULL) {
        if (current->number == key) {
            if (previous == NULL) {
                *head = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            return;
        }
        previous = current;
        current = current->next;
    }

    printf("Error: Key not found in the list\n");
}

// Function to delete a node with a specific value
void deleteByValue(struct Node **head, int value) {
    struct Node *current = *head;
    struct Node *previous = NULL;

    while (current != NULL) {
        if (current->number == value) {
            if (previous == NULL) {
                *head = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            return;
        }
        previous = current;
        current = current->next;
    }

    printf("Error: Value not found in the list\n");
}

// Function to insert a node with a specific value after a node with a specific key
void insertAfterKey(struct Node **head, int key, int newValue) {
    struct Node *current = *head;

    while (current != NULL) {
        if (current->number == key) {
            struct Node *newNode = createNode(newValue);
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }

    printf("Error: Key not found in the list\n");
}

// Function to insert a node with a specific value after a node with a specific value
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *current = *head;
    struct Node *previous = NULL;

    while (current != NULL) {
        if (current->number == searchValue) {
            struct Node *newNode = createNode(newValue);
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }

    printf("Error: Value not found in the list\n");
}

int main()
{
    struct Node *head = NULL;
    int choice, data, key, newValue;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by key\n");
        printf("5. Delete by value\n");
        printf("6. Insert after key\n");
        printf("7. Insert after value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &data);
            deleteByKey(&head, data);
            break;
        case 5:
            printf("Enter value to delete: ");
            scanf("%d", &data);
            deleteByValue(&head, data);
            break;
        case 6:
            printf("Enter key and value to insert: ");
            scanf("%d %d", &key, &newValue);
            insertAfterKey(&head, key, newValue);
            break;
        case 7:
            printf("Enter search value and new value to insert: ");
            scanf("%d %d", &data, &newValue);
            insertAfterValue(&head, data, newValue);
            break;
        case 8:
            return 0;
        default:
            printf("Invalid choice. Please try again.\n");
            break;
        }
    }
    return 0;
}
// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.
