#include <stdio.h>
#include <math.h>
int main(){
    double a, b, c, d, r1, r2, realpart, imagpart;
  
    printf("Enter value of a: ");
    scanf("%lf", &a);

    // For an equation to be a quadratic, the co-efficient of x^2 shouldn't be a zero 
    // Therefore the while loop is to make sure that the user inputs any value otherthan a zero before continuing
    while (a == 0)
    {
        printf("The value of a can't be a zero!\n");
        printf("\nEnter value of a: ");
        scanf("%lf", &a);
    }
    //user input of values a and b
    printf("Enter value of b: ");
    scanf("%lf", &b);

    printf("Enter value of c: ");
    scanf("%lf", &c);

    // Calculate the discriminate d and obtaining the square root of the result
    d = (b*b-(4*a*c));

    //printf("Value of d = %.1lf", d);

    // COndition for real distinct roots
    if (d > 0){
        r1 = (-b + sqrt(d))/(2*a);
        r2 = (-b - sqrt(d))/(2*a);
        printf("\nr1 = %.4lf", r1);
        printf("\nr2 = %.4lf", r2);
        }

    // Condition for repeatitive roots
    else if (d == 0){
        r1 = (-b)/(2*a);
        r2 = (-b)/(2*a);
        printf("Roots are equal:\n");
        printf("r1 = r2 = %.4lf", r1);
    }

    // Condition for non real roots that is if the discriminate is less than zero
    else {
        realpart = (-b)/(2*a);
        imagpart = sqrt(-d)/(2*a);

        printf("\nComplex roots\n");
        printf("The complex roots are: %.4lf + %.4lfi and %.4lf - %.4lfi\n", realpart, imagpart, realpart, imagpart);
    }
    return 0;
}
